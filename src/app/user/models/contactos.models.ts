export interface Contactos {
  id: number;
  name: string;
  phone: number;
  email: string;
}
