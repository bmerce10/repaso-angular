import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Contactos } from '../models/contactos.models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css'],
})
export class ContactosComponent implements OnInit {
  constructor(
    private loginService: LoginService,
    private _userService: UserService
  ) {}

  listContactos!: Contactos[];

  ngOnInit(): void {
    this.getContactos();
  }

  getContactos() {
    this._userService.getContactos().subscribe({
      next: (data: Contactos[]) => {
        this.listContactos = data;

        console.log('CARGANDO', this.listContactos);
      },
      error: (err) => alert('No se pudo cargar los datos'),
    });
  }

  isAuth() {
    return this.loginService.isAuth();
  }

  logOut() {
    this.loginService.signOut();
  }

  deleteContacto(i: number) {
    const contactos = this.listContactos.splice(i, 1);
    this._userService.setContactos(contactos);
    this._userService.deleteContacto(i).subscribe({
      next: () => alert('Se elimino el contacto correctamente'),
      error: (err) => {
        alert('No se pudo eliminar el contacto seleccionado');
        console.log(err);
      },
      complete: () => {
        if (this.listContactos != null) {
          this._userService.recargarContactos(this.listContactos).subscribe({
            next: (dat) => {
              console.log('se acomodo los datos', dat);
              this.getContactos();
            },
          });
        }
      },
    });
  }
}
