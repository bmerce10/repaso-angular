import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  getAuth,
  getIdToken,
  signInWithEmailAndPassword,
  signOut,
  updateCurrentUser,
} from 'firebase/auth';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private router: Router) {}

  token: string = null;
  uid: string = null;

  login(email: string, password: string) {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        this.uid = user.uid;
        user.getIdToken().then((token) => {
          this.token = token;
          this.router.navigate(['/user/contactos']);
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
      });

    // firebase
    //   .auth()
    //   .signInWithEmailAndPassword(email, password)
    //   .then((response) => {
    //     firebase
    //       .auth()
    //       .currentUser.getIdToken()
    //       .then((token) => {
    //         this.token = token;
    //       });
    //   });
  }

  getIdToken() {
    return this.token;
  }

  getUID() {
    return this.uid;
  }

  signOut() {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        this.token = null;
        this.router.navigate(['/user/login']);
      })
      .catch((error) => {
        window.alert(`Error al cerrar sesión: ${error}`);
      });
  }

  isAuth() {
    return this.token != null;
  }
}
