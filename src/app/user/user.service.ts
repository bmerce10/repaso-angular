import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';
import { Contactos } from './models/contactos.models';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  contactos: Contactos[] = [];

  constructor(private http: HttpClient, private loginService: LoginService) {}

  setContactos(contactos: Contactos[]) {
    this.contactos = contactos;
  }

  postContacto(contacto: Contactos): Observable<Contactos[]> {
    const token = this.loginService.getIdToken();
    const uid = this.loginService.getUID();
    if (this.contactos == null) {
      this.contactos = [];
    }

    return this.http.put<Contactos[]>(
      `https://repaso-angular-default-rtdb.firebaseio.com/${uid}.json?auth=${token}`,
      this.contactos
    );
  }

  getContactos(): Observable<Contactos[]> {
    const token = this.loginService.getIdToken();
    const uid = this.loginService.getUID();
    return this.http.get<Contactos[]>(
      `https://repaso-angular-default-rtdb.firebaseio.com/${uid}.json?auth=${token}`
    );
  }

  deleteContacto(index: number): Observable<Contactos> {
    const token = this.loginService.getIdToken();
    const uid = this.loginService.getUID();
    return this.http.delete<Contactos>(
      `https://repaso-angular-default-rtdb.firebaseio.com/${uid}/${index}.json?auth=${token}`
    );
  }

  recargarContactos(contactos: Contactos[]): Observable<Contactos[]> {
    const token = this.loginService.getIdToken();
    const uid = this.loginService.getUID();
    return this.http.put<Contactos[]>(
      `https://repaso-angular-default-rtdb.firebaseio.com/${uid}.json?auth=${token}`,
      this.contactos
    );
  }
}
