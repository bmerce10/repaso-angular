import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { LoginService } from './login.service';
import { UserService } from './user.service';
import { LoginComponent } from './login/login.component';
import { ContactosComponent } from './contactos/contactos.component';
import { VigilanteGuard } from './vigilante.guard';
import { AgregarContactoComponent } from './agregar-contacto/agregar-contacto.component';

@NgModule({
  declarations: [LoginComponent, ContactosComponent, AgregarContactoComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [UserService, LoginService, VigilanteGuard],
})
export class UserModule {}
