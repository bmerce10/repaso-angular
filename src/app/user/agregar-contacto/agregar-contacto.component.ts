import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contactos } from '../models/contactos.models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-agregar-contacto',
  templateUrl: './agregar-contacto.component.html',
  styleUrls: ['./agregar-contacto.component.css'],
})
export class AgregarContactoComponent implements OnInit {
  form: FormGroup = this.formBuilder.group({
    id: ['', [Validators.required]],
    name: ['', [Validators.required]],
    phone: ['', [Validators.required, Validators.minLength(9)]],
    email: ['', [Validators.required, Validators.email]],
  });

  contactos: Contactos[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private _userService: UserService
  ) {}

  ngOnInit(): void {}

  getContactos() {
    this._userService.getContactos().subscribe({
      next: (data) => (this.contactos = data),
    });
  }

  addContacto() {
    if (this.form.valid) {
      const contacto = this.form.value;
      this.getContactos();
      this.contactos.push(contacto);
      this._userService.setContactos(this.contactos);
      this._userService.postContacto(contacto).subscribe({
        next: (data) => {
          console.log('se agrego', data);
        },
        error: (err) => {
          alert(`Error al crear contacto`);
          console.log(err);
        },
      });
    } else {
      alert('Campos sin completar');
    }
  }
}
