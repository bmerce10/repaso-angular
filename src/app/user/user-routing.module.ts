import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ContactosComponent } from './contactos/contactos.component';
import { VigilanteGuard } from './vigilante.guard';

const routes: Routes = [
  {
    path: '',
    component: ContactosComponent,
    canActivate: [VigilanteGuard],
  },

  {
    path: 'contactos',
    component: ContactosComponent,
    canActivate: [VigilanteGuard],
  },

  {
    path: 'login',
    component: LoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
