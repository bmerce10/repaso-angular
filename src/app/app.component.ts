import { Component, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { getDatabase } from 'firebase/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'repaso';

  constructor() {}

  ngOnInit(): void {
    const app = initializeApp({
      apiKey: 'AIzaSyDvgGhY9BdV7SlAhLC6a0IcAYfZ3BgQMOY',
      authDomain: 'repaso-angular.firebaseapp.com',
      databaseURL: 'https://repaso-angular-default-rtdb.firebaseio.com',
      projectId: 'repaso-angular',
      storageBucket: 'repaso-angular.appspot.com',
      messagingSenderId: '492531102651',
      appId: '1:492531102651:web:61af49c6da021cfe67ca9e',
    });
    const database = getDatabase(app);
  }
}
